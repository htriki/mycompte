import React from 'react';

const ContextCategorie = React.createContext({
    categorieSelected: {},
    changeValue:(e)=>{}
});

export const CategorieProvider = ContextCategorie.Provider;
export const CategorieConsumer = ContextCategorie.Consumer;

export default ContextCategorie;
