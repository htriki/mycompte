import React, { Component } from "react";
const { Provider, Consumer } = React.createContext();

class ContextCategorieProvider extends Component {

    state = {
        categories: [{id: 1, libelle: 'test catégorie 1'},
                     {id: 2, libelle: 'autre test de catégorie'}
                    ],
        categorieSelected: {id: 0,
                            libelle: ''
        },
        title: ''
        
    };

    changeValue = (e) => {
        let c = this.state.categorieSelected;
        c.libelle = e.target.value;
        this.changeCategorie(c);
    };

    changeCategorie = (c) => {

        let title = 'Nouvelle catégorie';
        if (c.id !== 0) {
            title = 'Modification de ' + c.libelle;
        }            
        this.setState({categorieSelected: c, title});        
    }

    read = () => {
        // Lecture des catégories
    }

    save = (categorie) => {
        // Sauvegarde d'une catégorie
    }

    delete = (categorie) => {
        // Suppression d'une catégorie
    }

    render() {
        return (
            <Provider
                value={
                        {categories:this.state.categories,
                         categorieSelected: this.state.categorieSelected, 
                         title: this.state.title,
                         changeValue: this.changeValue,
                         changeCategorie: this.changeCategorie,
                         read: this.read,
                         save: this.save,
                         delete: this.delete
                        }
                    }
            >
                {this.props.children}
            </Provider>
        );
    }
}

export { ContextCategorieProvider, Consumer as ContextCategorieConsumer };
