import React                    from 'react';
import {DataTable}              from 'primereact/datatable';
import {Column}                 from 'primereact/column';
import {Button}                 from 'primereact/button';
import DlgCategorie             from '../../components/dialog/dlgCategorie';

import {ContextCategorieProvider} from '../../store/categorieContext';
import {ContextCategorieConsumer} from '../../store/categorieContext';

import './categorie.css';

class Categorie extends React.Component {
 
    constructor() {
        super();
        this.state = {
            visibleDialog: false
        };

        this.closeDlg = this.closeDlg.bind(this);
    }

    doubleClick = (e) => {       
        this.setState({visibleDialog: true});
    }

    closeDlg() {
      this.setState({visibleDialog: false})
    }

    render()  {

      return (
        
        <ContextCategorieProvider>

          <ContextCategorieConsumer>
            {contextCategorie=>{

              return <div className="categorie">

                <DlgCategorie visible={this.state.visibleDialog} 
                              title={contextCategorie.title}
                              onClose={this.closeDlg}/>

                <h2>Catégorie</h2>

                <DataTable value={contextCategorie.categories}
                            paginator={true}
                            rows={10}
                            rowsPerPageOptions={[5,10,15,20]} 
                            selection={this.state.categorieSelected} 
                            onRowDoubleClick={
                              (e)=>{
                                contextCategorie.changeCategorie(Object.assign({}, e.data));
                                this.doubleClick(e)
                              }
                            }
                            emptyMessage="Aucune catégorie !">
                    <Column field="id" header="Id" style={{width:'10%'}} />
                    <Column field="libelle" header="Libellé"/>

                </DataTable>

                <div className="buttonZone">
                  <Button label="Ajouter une catégorie" 
                          onClick={()=>{
                            contextCategorie.changeCategorie({id:0, libelle:''});
                            this.setState({visibleDialog: true})}} />
                </div>
              
              </div>
            }}
          </ContextCategorieConsumer>          
        </ContextCategorieProvider>
        
      );
    }
  }

  export default Categorie;
