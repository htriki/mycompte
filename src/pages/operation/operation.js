import React                    from 'react';
import {DataTable}              from 'primereact/datatable';
import {Column}                 from 'primereact/column';
import {Dropdown}               from 'primereact/dropdown';
import {InputText}              from 'primereact/inputtext';

import './operation.css';

const mois = [ {value:0, label: '.'}, {value:1, label:'Janvier'}, {value: 2, label: 'Février'}];

class Operation extends React.Component {
 
    constructor() {
        super();
        this.state = {
            operationSelected: {},
            moisSelected: 0,
            anneeSelected: 0,
            operations:[
              {
                id:1, 
                libelle: 'xxxxx',
                dateOperation: '11/01/2019',
                categorie: {id: 1, libelle: 'ma catégorie'},
                sousCategorie: {id: 2, 
                                libelle: 'ma sous catégorie', idCategorie: 1, 
                                categorie: {id: 1, libelle:'Categorie'}},
                montant: 900,
                solde: -100,
                encaisse: true,
                pointe: true,
                commentaire: ''
              },
              {
                id:1, libelle: 'xxxxx', dateOperation: '11/01/2019', categorie: {id: 1, libelle: 'ma catégorie'},
                sousCategorie: {id: 2, libelle: 'ma sous catégorie', idCategorie: 1, categorie: {id: 1, libelle:'Categorie'}},
                montant: 900, solde: -100, encaisse: true, pointe: true, commentaire: ''
              },
              {
                id:1, libelle: 'xxxxx', dateOperation: '11/01/2019', categorie: {id: 1, libelle: 'ma catégorie'},
                sousCategorie: {id: 2, libelle: 'ma sous catégorie', idCategorie: 1, categorie: {id: 1, libelle:'Categorie'}},
                montant: 900, solde: -100, encaisse: true, pointe: true, commentaire: ''
              }              
              ,
              {
                id:1, libelle: 'xxxxx', dateOperation: '11/01/2019', categorie: {id: 1, libelle: 'ma catégorie'},
                sousCategorie: {id: 2, libelle: 'ma sous catégorie', idCategorie: 1, categorie: {id: 1, libelle:'Categorie'}},
                montant: 900, solde: -100, encaisse: true, pointe: true, commentaire: ''
              }
        ]};


    }

    changeMois(e) {
      this.setState({moisSelected: e.value});
    }

    doubleClick = () => {
        console.log("OK");
    }

    render()  {
      return (
        <div className="operation">

            <h2>Opérations</h2>

            <div className="p-grid header">
                <div className="p-col-1 colLeft">Mois :</div>              
                <div className="p-col-2">
                    <Dropdown value={this.state.moisSelected} options={mois} 
                              onChange={(e)=>this.changeMois(e)} 
                              />                
                </div>              
                <div className="p-col-1 colLeft">Année :</div>
                <div className="p-col-2">
                    <InputText type="number"  value={this.state.anneeSelected} 
                    maxLength="4" onChange={(e) => this.setState({anneeSelected: e.target.value})} />
                </div>
                <div className="p-col-1 colLeft">Solde :</div>
                <div className="p-col-1 colRight">321321</div>
                <div className="p-col-1 colLeft">Encaissé :</div>
                <div className="p-col-1 colRight">321321</div>
                <div className="p-col-1 colLeft">Pointé :</div>
                <div className="p-col-1 colRight">321321</div>
            </div>

            <DataTable value={this.state.operations}
                        paginator={true}
                        rows={10}
                        rowsPerPageOptions={[5,10,15,20]} 
                        selection={this.state.operationSelected} 
                        onRowDoubleClick={(e)=>{this.doubleClick()}}
                        emptyMessage="Aucune catégorie !">
                <Column field="id" header="Id" style={{width:'10%'}} />
                <Column field="libelle" header="Libellé" />
                <Column field="categorie.libelle" header="Catégorie" />
                <Column field="sousCategorie.libelle" header="Sous Catégorie" />
                <Column field="montant" header="Montant"  style={{width:'10%'}}/>
                <Column field="solde" header="Solde"      style={{width:'10%'}}/>
                <Column field="encaisse" header="E" style={{width: '3%'}} />
                <Column field="pointe" header="Pt"  style={{width: '3%'}}/>                
            </DataTable>

            <br/>
            <div className="p-grid montant">
                <div className="p-col-1 p-offset-10 colLeft">Solde : </div>
                <div className="p-col-1 right colRight">321321</div>
                <div className="p-col-1 p-offset-10 colLeft">Solde Encaissé :</div>
                <div className="p-col-1 right colRight">321321</div>
                <div className="p-col-1 p-offset-10 colLeft">Solde Pointé :</div>
                <div className="p-col-1 right colRight">321321</div>
            </div>


        </div>
      );
    }
  }

  export default Operation;