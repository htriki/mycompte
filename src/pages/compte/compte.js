import React                    from 'react';
import {DataTable}              from 'primereact/datatable';
import {Column}                 from 'primereact/column';
import {Button}                 from 'primereact/button';
import DlgCompte             from '../../components/dialog/dlgCompte';

import './compte.css';

class Compte extends React.Component {
 
    constructor() {
        super();
        this.state = {
            visibleDialog: false,
            titleDialog: '',
            compteSelected: {},
            comptes:[
            {id:1, libelle: 'CréditMutuel', titulaire: 'H. et J.', numero: '', commentaire: ''},
            {id:2, libelle: 'BPROP', titulaire: 'H. et J.', numero: '', commentaire: ''},
            {id:3, libelle: 'xsdfgsdfgf', titulaire: 'Emma', numero: '', commentaire: ''}
        ]};
    }

    addCcompte() {
      alert("ajouter !");
    }

    doubleClick = (e) => {
        this.setState({titleDialog: 'Modification de ' + e.data.libelle, visibleDialog: true})  
    }

    closeDlg() {
      this.setState({visibleDialog: false})
    }

    renderDlg() {
      return (
        <DlgCompte  title={this.state.titleDialog}
                    visible={this.state.visibleDialog} 
                    onClose={()=>this.closeDlg()} />
      );
    }

    render()  {
      return (
        <div className="compte">

            <h2>Compte</h2>

            {this.renderDlg()}

            <DataTable value={this.state.comptes}
                        paginator={true}
                        rows={10}
                        rowsPerPageOptions={[5,10,15,20]} 
                        selection={this.state.compteSelected} 
                        onRowDoubleClick={(e)=>{this.doubleClick(e)}}
                        emptyMessage="Aucun compte !">
                <Column field="id" header="Id" style={{width:'8%'}} />
                <Column field="libelle" header="Libellé"/>
                <Column field="numero" header="Numéro"/>
                <Column field="titulaire" header="Titulaire du compte"/>
            </DataTable>

            <div className="buttonZone">
              <Button label="Ajouter un compte" 
                      onClick={()=>this.setState({titleDialog: 'Nouveau compte', visibleDialog: true})} />
            </div>

        </div>
      );
    }
  }

  export default Compte;