import React                    from 'react';
import {DataTable}              from 'primereact/datatable';
import {Column}                 from 'primereact/column';
import {Button}                 from 'primereact/button';

import './sousCategorie.css';

class SousCategorie extends React.Component {
 
    constructor() {
        super();
        this.state = {
            visibleDialog: false,
            sousCategorieSelected: {},
            sousCategories:[
            {id:1, categorie:{id: 1, libelle:'test'}, libelle: 'xxxxx'},
            {id:2, categorie:{id: 1, libelle:'test'}, libelle: 'xxggg'},
            {id:3, categorie:{id: 1, libelle:'test'}, libelle: 'xsdfgsdfgf'},
            {id:4, categorie:{id: 1, libelle:'test'}, libelle: 'sdflmgsdfmgl'}                              
        ]};
    }

    doubleClick = () => {
        console.log("OK");
    }

    render()  {
      return (
        <div className="sousCategorie">

            <h2>Sous Catégorie</h2>


            <DataTable  value={this.state.sousCategories}
                        paginator={true}
                        rows={10}
                        rowsPerPageOptions={[5,10,15,20]} 
                        selection={this.state.sousCategories} 
                        onRowDoubleClick={(e)=>{this.doubleClick()}}
                        emptyMessage="Aucune sous Catégorie !">

                <Column field="id" header="Id" style={{width:'10%'}} />
                <Column field="libelle" header="Libellé"/>
                <Column field="categorie.libelle" header="Catégorie" />

            </DataTable>

            <div className="buttonZone">

              <Button   label="Ajouter une sous catégorie" 
                        onClick={()=>this.setState({visibleDialog: true})} />

            </div>

        </div>
      );
    }
  }

  export default SousCategorie;