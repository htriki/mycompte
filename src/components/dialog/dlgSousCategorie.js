import React                    from 'react';
import {Dialog}                 from 'primereact/dialog';
import {Button}                 from 'primereact/button';
import {InputText}              from 'primereact/inputtext';


class DlgSousCategorie extends React.Component {

    constructor() {
        super();

        this.state = {
            id: 0,
            libelle: ''
        }
    } 

    render()  {

        const footer = (
            <div>
                <Button label="OK"      icon="pi pi-check" />
                <Button label="Annuler" icon="pi pi-times" 
                        onClick={this.props.onClose} />
            </div>
        );        

        return (
            <div className="dlgCategorie">

                <Dialog header="Nouvelle Sous Catégorie" footer={footer} 
                        visible={this.props.visible} 
                        style={{width: '50vw'}} 
                        modal={true} 
                        onHide={this.props.onClose}>

                    <div className="p-grid">
                        <div className="p-col-4 colLeft">Identifiant :</div>
                        <div className="p-col-8">
                            <InputText  value={this.state.id} 
                                        disabled={true}
                                        onChange={(e) => this.setState({id: e.target.value})} />
                        </div>
                    </div>
                    <div className="p-grid">
                        <div className="p-col-4 colLeft">Libellé :</div>
                        <div className="p-col-8">
                            <InputText  value={this.state.libelle} 
                                        className="input"
                                        maxLength="255"
                                        onChange={(e) => this.setState({libelle: e.target.value})} />
                        </div>
                    </div>

                </Dialog>
            </div>
        );
    }
  }

  export default DlgCategorie;