import React                    from 'react';
import {Dialog}                 from 'primereact/dialog';
import {Button}                 from 'primereact/button';
import {InputText}              from 'primereact/inputtext';
import { CategorieConsumer }    from '../../store/contextCategorie';
import {ContextCategorieConsumer} from '../../store/categorieContext';

import './dlg.css';

class DlgCategorie extends React.Component {

    render()  {

        const footer = (
            <div>
                <Button label="OK"      icon="pi pi-check" />
                <Button label="Annuler" icon="pi pi-times" />
            </div>
        );        

        return (

            <ContextCategorieConsumer>
            {context=>{
                //console.log("context", context);
                return <div className="dlg">

                            <Dialog header={context.title} 
                                    footer={footer} 
                                    visible={this.props.visible} 
                                    style={{width: '50vw'}} 
                                    modal={true} 
                                    onHide={this.props.onClose}>

                                {
                                    (context.categorieSelected.id !== 0)
                                    ?
                                        <div className="p-grid">
                                            <div className="p-col-4 colLeft">Identifiant :</div>
                                            <div className="p-col-8 bold">
                                                {context.categorieSelected.id}
                                            </div>
                                        </div>
                                    : ''
                                }
                                <div className="p-grid">
                                    <div className="p-col-4 colLeft">Libellé :</div>
                                    <div className="p-col-8">
                                        <InputText  value={context.categorieSelected.libelle}
                                                    onChange={(e)=>context.changeValue(e)}
                                                    className="input"
                                                    maxLength="255" />
                                    </div>
                                </div>

                            </Dialog>
                            
                        </div>
            }}
            </ContextCategorieConsumer>
        );
    }
  }

  export default DlgCategorie;
