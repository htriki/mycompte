import React, {Component}       from 'react';
import {Menubar}                from 'primereact/menubar';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import { withRouter } from 'react-router-dom';

@withRouter
class Menu extends Component {

    constructor() {

        super();
        this.state = {
            items:[
                {
                   label:'Menu',
                   icon:'pi pi-fw pi-bars',
                   items:[
                      {
                          label: 'Technique',
                          icon:'pi pi-fw pi-cog',
                          items:[
                              {
                                label:'Compte',
                                icon:'pi pi-fw pi-list',
                                command:()=>{this.props.history.push('/compte')}
                              },
                              {
                                label:'Catégorie',
                                icon:'pi pi-fw pi-list',
                                command:()=>{this.props.history.push('/categorie')}
                             },
                              {
                                label: 'Sous Catégorie',
                                icon:'pi pi-fw pi-arrow-circle-right',
                                command: ()=>{this.props.history.push('/souscategorie')}
                              }
                          ]
                      },                      
                      {
                        label:'Opérations',
                        icon:'pi pi-fw pi-external-link',
                        command:()=>{this.props.history.push('/operation')}
                      },                      
                      {
                         separator:true
                      },
                      {
                         label:'A propos de ...',
                         icon:'pi pi-fw pi-comment',
                         command: ()=>{this.props.history.push('/about')}
                      }
                   ]
                }
                
             ]
        };

    }

    render() {

        return (
		
			<Menubar model={this.state.items} className="menu" >
			</Menubar>
        );
    }

}

export default Menu;