import React                        from 'react';
import { Route, Switch }            from 'react-router-dom';
import { Redirect }                 from 'react-router-dom';
import { BrowserRouter }            from 'react-router-dom';

import PageAbout                    from '../../pages/about/about';
import PageHome                     from '../../pages/home/home';
import PageCategorie                from '../../pages/categorie/categorie';
import PageOperation                from '../../pages/operation/operation';
import PageSousCategorie            from '../../pages/sousCategorie/sousCategorie';
import PageCompte                   from '../../pages/compte/compte';

import Menu                         from './menu.js';

const MyMenu = () => (
    <div>
        <Menu />
    </div>    
)


class Init extends React.Component {

    render() {

        return (
            <BrowserRouter basename="/#" >   


                <div>
                    <Route path="/"     component={MyMenu}              />

                    {
                        <Switch>
                            
                            <Route path="/about"            component={PageAbout}           />
                            <Route path="/categorie"        component={PageCategorie}       />
                            <Route path="/home"             component={PageHome}            />
                            <Route path="/operation"        component={PageOperation}       />
                            <Route path="/souscategorie"    component={PageSousCategorie}   />
                            <Route path="/compte"           component={PageCompte}          />

                            <Redirect from='/' exact={true} to="/home"  />

                        </Switch>
                    }
                </div>
            </BrowserRouter>
        )
    }
}

export default Init;